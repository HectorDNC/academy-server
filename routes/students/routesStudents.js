module.exports = function(router) {
    router.get('/students/get', (req, res) => { //Listar Alumnos
        req.getConnection((err, conn) => {
            if (err) return res.status(500).send('Error en el servidor') //Si ocurre un error en la conexión a la BD
            conn.query(`SELECT *, DATE_FORMAT(dateBirth, "%d-%m-%Y") as dateBirthFormated, DATE_FORMAT(dateBirth, "%Y-%m-%d") as dateBirth 
                FROM students ORDER BY status DESC, id DESC`, (err, students) => { //Consulta
                if (err) return res.status(500).send(err.sqlMessage) //Si ocurre un error en la consulta
                res.json(students)
            })
        })
    })
    router.post('/students/insert', (req, res) => { //Registrar Alumno
        req.getConnection((err, conn) => {
            if (err) return res.status(500).send('Error en el servidor')
            const { name, lastName, dateBirth, address, section } = req.body
            var student = { name, lastName, dateBirth, address, section }
            conn.query('INSERT INTO students SET ?', [student], (err, response) => {
                if (err) return res.status(500).send(err.sqlMessage)
                res.send(true)
            })
        })
    })
    router.post('/students/update', (req, res) => { //Actualizar Alumno
        req.getConnection((err, conn) => {
            if (err) return res.status(500).send('Error en el servidor')
            const { id, name, lastName, dateBirth, address, section } = req.body
            var student = { name, lastName, dateBirth, address, section }
            conn.query('UPDATE students SET ? WHERE id = ?', [student, id], (err, response) => {
                if (err) return res.status(500).send(err.sqlMessage)
                res.send(true)
            })
        })
    })
    router.post('/students/disable/:id', (req, res) => { //Deshabilitar Alumno
        req.getConnection((err, conn) => {
            if (err) return res.status(500).send('Error en el servidor')
            const id = req.params.id
            conn.query('UPDATE students SET status = 0 WHERE id = ?', [id], (err, response) => {
                if (err) return res.status(500).send(err.sqlMessage)
                res.send(true)
            })
        })
    })
    router.post('/students/enable/:id', (req, res) => { //Habilitar Alumno
        req.getConnection((err, conn) => {
            if (err) return res.status(500).send('Error en el servidor')
            const id = req.params.id
            conn.query('UPDATE students SET status = 1 WHERE id = ?', [id], (err, response) => {
                if (err) return res.status(500).send(err.sqlMessage)
                res.send(true)
            })
        })
    })
}
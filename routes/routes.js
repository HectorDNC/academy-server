const express = require('express')
const cors = require('cors')
const router = express.Router()

require('./students/routesStudents')(router)

router.get('/', (req, res) => {
    res.send('Academy')
})

module.exports = router
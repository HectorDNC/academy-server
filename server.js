const express = require('express')
const mysql = require('mysql')
const myconn = require('express-myconnection')
const cors = require('cors')
const routes = require('./routes/routes')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.json());

var whiteList = ['http://localhost:3000']

var corsOptions = {
    origin: function(origin, callback) {
        if (whiteList.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Acceso denegado'))
        }
    }
}
app.use(cors(corsOptions))

app.use(myconn(mysql, {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'informatica',
    database: 'academy'
}))

app.use(routes)

app.listen(9000, () => {
    console.log('Hello!', 'http://localhost:' + 9000)
})